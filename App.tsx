import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import Routes from './src/routes';

export default function App() {
  
  return (
    <NavigationContainer>
      <StatusBar backgroundColor='#442D2D' style="light" />
        <View style={{ flex: 1, backgroundColor: '#312e38' }}>
          <Routes />
        </View>
    </NavigationContainer>
  );
}
