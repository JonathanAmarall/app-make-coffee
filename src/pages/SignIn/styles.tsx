import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
  justify-content: center;
  padding: 20px; 
`;

export const Input = styled.TextInput`
  width: 100%;
  height: 45px;
  border: 1px solid #442D2D;
  margin-bottom: 15px;
  padding: 15px;
`;

export const Title = styled.Text`
  font-size: 24px;
  text-align: left;
  margin: 10px;
`;

export const ButtonLogin = styled.TouchableOpacity`
  margin-top:15px;
  background-color: #442d2d;
  color: #fff;
  height: 55px;
  display: flex;
  justify-content: center;
  align-items:center;
  border-radius: 4px;
`;

export const ButtonLoginText = styled.Text`
  color: #fff;
  font-size: 16px;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;
`;
