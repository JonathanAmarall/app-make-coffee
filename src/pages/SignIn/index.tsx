import React from 'react';
import { View, Image } from 'react-native';

import { Container, Input, Title, ButtonLogin ,ButtonLoginText} from './styles';
import { useNavigation } from '@react-navigation/native';

const SignIn: React.FC = () => {
  const navigate = useNavigation();
  return (
    <Container>
      <View style={{alignSelf: 'center'}}>
        <Image source={{
          width: 150, height: 150,
          uri:'https://cdn0.iconfinder.com/data/icons/control-pad-shortcuts-rounded/100/Bar-Coffee_03-512.png'
        }} />

      </View>
      <Title>Login</Title>
      <Input placeholder="Email" placeholderTextColor="#666"/>
      <Input placeholder="Password" placeholderTextColor="#666" />
      <ButtonLogin onPress={() => {
          navigate.navigate('Dashboard')
          }}>
        <ButtonLoginText>Entrar</ButtonLoginText>
      </ButtonLogin>
    </Container>);
}

export default SignIn;
