import React from "react";
import styled from "styled-components/native";

export const Container = styled.View`
  padding: 15px;
  flex: 1;
  background-color: #fff;
  justify-content: space-evenly;
`;
