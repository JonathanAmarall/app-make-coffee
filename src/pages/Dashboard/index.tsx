import React from "react";
import { View, Text, Image, ScrollView } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

import {
  Container,
  Title,
  StandOultMonth,
  TimeLine,
  TitleCoffeMonth,
  AuthorRecipeMonth,
  AuthorRecipeMonthAvatar,
  AuthorRecipeMonthName,
  TimeLineTitle,
} from "./styles";
import { TouchableOpacity } from "react-native-gesture-handler";

import TimeLineCard from "../../components/Card";

interface Props {
  navigation: any;
}

const Dashboard: React.FC<Props> = ({ navigation }) => {
  return (
    <ScrollView>
      <Container>
        <>
          <TouchableOpacity
            style={{ marginTop: 10 }}
            onPress={() => {
              navigation.openDrawer();
            }}
          >
            <Text>
              <MaterialIcons name="menu" size={30} color="#000" />
            </Text>
          </TouchableOpacity>
          <View>
            <Title>Make Coffee</Title>
            <TitleCoffeMonth>Café do mês de maio</TitleCoffeMonth>
            <StandOultMonth>
              <Image
                source={{
                  width: 330,
                  height: 200,
                  uri:
                    "https://b.zmtcdn.com/data/pictures/chains/3/18415073/1827f2eea52a91996f2d37a946aa04b7.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
                }}
              />
            </StandOultMonth>
          </View>

          <AuthorRecipeMonth>
            <AuthorRecipeMonthAvatar
              source={{
                uri:
                  "https://scontent.fsqx1-1.fna.fbcdn.net/v/t1.0-9/117639496_3116771961776928_4321559081040474813_o.jpg?_nc_cat=106&_nc_sid=09cbfe&_nc_ohc=JV94oFuy0iMAX--Dt8X&_nc_ht=scontent.fsqx1-1.fna&oh=30d3cd85d005e8916c86d3676a2e303e&oe=5F5E1F64",
              }}
            />
            <AuthorRecipeMonthName>Sarah Mattos</AuthorRecipeMonthName>
          </AuthorRecipeMonth>

          <TimeLine>
            <TimeLineTitle>Linha do tempo</TimeLineTitle>
            <TimeLineCard
              side={0}
              name="Jonathan"
              order_title="Será o próximo a fazer o café"
              avatar="https://lh3.googleusercontent.com/ogw/ADGmqu-3pE01iA2kCqB5XAgBwd-RJVPuSrEdM11uetZ3=s83-c-mo"
            />
            <TimeLineCard
              side={1}
              name="André"
              order_title="Foi o último a fazer o café"
              avatar="https://scontent.fsqx1-1.fna.fbcdn.net/v/t31.0-8/26232705_864507907052388_8247508254274851675_o.jpg?_nc_cat=106&_nc_sid=09cbfe&_nc_ohc=7BGEvkWHgn4AX8yWBNG&_nc_ht=scontent.fsqx1-1.fna&oh=1d94cc27703caf028a39460200aca704&oe=5F5DDEE6"
            />
            <TimeLineCard
              side={2}
              name="Tobias"
              order_title="É o terceiro da fila"
              avatar="https://scontent.fsqx1-1.fna.fbcdn.net/v/t1.0-9/95358348_2613543645600611_6204547057592565760_o.jpg?_nc_cat=110&_nc_sid=09cbfe&_nc_ohc=TWp4-pPFPGsAX9UJqNL&_nc_ht=scontent.fsqx1-1.fna&oh=27939d1a33b60e136b8de0362e6475ba&oe=5F5DB22F"
            />
          </TimeLine>
        </>
      </Container>
    </ScrollView>
  );
};

export default Dashboard;
