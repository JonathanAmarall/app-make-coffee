import React from "react";
import styled from "styled-components/native";

export const Container = styled.View`
  padding: 15px;
  flex: 1;
  background-color: #fff;
  justify-content: space-evenly;
`;

export const Title = styled.Text`
  text-align: center;
  font-size: 30px;
  margin: 5px;
`;

export const StandOultMonth = styled.View`
  background-color: #ccc;
  width: 100%;
  height: 200px;
  margin-bottom: 10px;
`;

export const TitleCoffeMonth = styled.Text`
  font-size: 12px;
  font-weight: bold;
`;

export const AuthorRecipeMonth = styled.View`
  flex-direction: row;
`;

export const AuthorRecipeMonthAvatar = styled.Image`
  width: 30px;
  height: 30px;
  border-radius: 15px;
  background-color: #666;
`;

export const AuthorRecipeMonthName = styled.Text`
  margin: 5px;
`;

export const TimeLine = styled.View`
  width: 100%;
  height: 700px;
  flex: 1;
  align-items: center;
  margin-top: 5px;
`;

export const TimeLineTitle = styled.Text`
  letter-spacing: 5px;
  padding: 5px;
`;
