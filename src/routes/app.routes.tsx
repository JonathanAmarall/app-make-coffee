import React from "react";
import { View, Text } from 'react-native';
import { createDrawerNavigator } from "@react-navigation/drawer";

import Dashboard from "../pages/Dashboard";
import Rating from "../pages/Rating";
import Recipe from "../pages/Recipe";
import DrawerContent from '../components/DrawerContent';
const App = createDrawerNavigator();
// import { Container } from './styles';



const AppRoutes: React.FC = () => {
  return (
    <App.Navigator drawerContent={(props) => <DrawerContent {...props} />  }>
      <App.Screen name="Dashboard" component={Dashboard} />
      <App.Screen name="Rating" component={Rating} />
      <App.Screen name="Recipe" component={Recipe} />
    </App.Navigator>
  );
};

export default AppRoutes;
