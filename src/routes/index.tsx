import React from 'react';
import { View } from 'react-native';

import AuthRoutes from './auth.routes';
import AppRoutes from './app.routes';

// import { Container } from './styles';

const Routes: React.FC = () => {
  const isSignIn: boolean = false;

  return isSignIn ? <AuthRoutes /> : <AppRoutes />;
};

export default Routes;
