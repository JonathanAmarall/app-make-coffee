import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';

import { View, StyleSheet } from 'react-native';
import {
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  TouchableRipple,
  Switch,
  Text
} from 'react-native-paper';
import { MaterialCommunityIcons as Icon } from "@expo/vector-icons";

import { DrawerContentScrollView, DrawerItem, DrawerContentComponentProps  } from '@react-navigation/drawer';

const DrawerContent: React.FC<DrawerContentComponentProps> = (props) => {
  // const nav = useNavigation();
  const [isDarkTheme, setIsDarkTheme] = useState(false);

  const handleToggleTheme = () => {
    console.log(isDarkTheme);
    setIsDarkTheme(!isDarkTheme);
  }


  return (
    <View style={{flex: 1}}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View style={{flexDirection: 'row', marginTop: 15}}>
              <Avatar.Image
                source={{
                  uri:'https://lh3.googleusercontent.com/ogw/ADGmqu-3pE01iA2kCqB5XAgBwd-RJVPuSrEdM11uetZ3=s83-c-mo'
                }}
                size={50} />
                <View style={{marginLeft:15, flexDirection: 'column'}}>
                  <Title style={styles.title}>Jonathan Amaral</Title>
                  <Caption style={styles.caption}>@JonathanAmarall</Caption>
                </View>
            </View>
            <View style={styles.row}>
             <View style={styles.section}>
                <Paragraph style={[styles.paragraph, styles.caption]}>8</Paragraph>
                <Caption style={styles.caption}>Receitas</Caption>
              </View>
              <View style={styles.section}>
                <Paragraph style={[styles.paragraph, styles.caption]}>10</Paragraph>
                <Caption style={styles.caption}>Cafés feitos</Caption>
              </View>
            </View>
          </View>
        </View>

        <Drawer.Section style={styles.drawerSection}>
          <DrawerItem
             icon={({color, size}) => (
                <Icon
                name="home-outline"
                color={color}
                size={size}
                />
              )}
              activeBackgroundColor='#442d2d'
              activeTintColor="white"
              focused={true}
              label="Home"
              onPress={() => props.navigation.navigate('Dashboard')}
              />

          <DrawerItem
             icon={({color, size}) => (
                <Icon
                name="book-open-page-variant"
                color={color}
                size={size}
                />
              )}
              activeBackgroundColor='#442d2d'
              activeTintColor="white"
              focused={false}
              label="Minhas Receitas"
              onPress={() => props.navigation.navigate('Recipe')}
          />

          <DrawerItem
             icon={({color, size}) => (
                <Icon
                name="star-box"
                color={color}
                size={size}
              />
              )}
              activeBackgroundColor='#442d2d'
              activeTintColor="white"
                focused={false}
                label="Avaliações"
                onPress={() => props.navigation.navigate('Rating')}
            />

             <DrawerItem
             icon={({color, size}) => (
                <Icon
                name="settings-outline"
                color={color}
                size={size}
              />
              )}
              activeBackgroundColor='#442d2d'
              activeTintColor="white"
                focused={false}
                label="Configurações"
                onPress={() => {console.log('')}}
            />

            <DrawerItem
             icon={({color, size}) => (
                <Icon
                name="chat-outline"
                color={color}
                size={size}

              />
              )}
              activeBackgroundColor='#442d2d'
              activeTintColor="white"
                focused={false}
                label="Chat"
                onPress={() => {console.log('')}}
            />
        </Drawer.Section>
        {/* Preferences section */}
        <Drawer.Section title="Preferencias">
          <TouchableRipple onPress={() => { handleToggleTheme(); }} >
            <View style={styles.preference}>
              <Text>Dark theme</Text>
              <Switch color="#442d2d" value={isDarkTheme} />
            </View>
          </TouchableRipple>
        </Drawer.Section>
      </DrawerContentScrollView>
      {/* Sing out Section */}
      <Drawer.Section style={styles.bottomDrawerSection} >
        <DrawerItem
         icon={({color, size}) => (
          <Icon
          name="exit-to-app"
          color={color}
          size={size}
          />
      )}
          label="Sign Out"
          onPress={() => {}}
          />
      </Drawer.Section>
    </View>)
}

export default DrawerContent;


const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
      marginBottom: 15,
      borderTopColor: '#f4f4f4',
      borderTopWidth: 1
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
