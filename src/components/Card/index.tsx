import React from "react";
import { View, Text } from "react-native";

import { Container, TitleCard, ImageCard, OrderTitleCard } from "./styles";

interface CardProps {
  avatar?: string;
  name: string;
  order_title: string;
  side?: number; // left 0 right 1
}

const Card: React.FC<CardProps> = ({
  name,
  avatar,
  side = 0,
  order_title,
  ...rest
}) => {
  return (
    <Container
      keySide={side}
      {...rest}
      style={{
        borderBottomColor: "#ccc",
        borderBottomWidth: 1,
        paddingBottom: 10,
      }}
    >
      <View>
        <TitleCard>{name}</TitleCard>
        <OrderTitleCard>{order_title}</OrderTitleCard>
      </View>
      <View>
        <ImageCard
          source={{
            uri: avatar,
          }}
        />
      </View>
    </Container>
  );
};

export default Card;
