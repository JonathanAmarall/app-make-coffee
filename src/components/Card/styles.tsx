import React from "react";

import styled, { css } from "styled-components/native";

interface ContainerProps {
  keySide: number;
}

export const Container = styled.View<ContainerProps>`
  height: auto;
  width: 100%;
  background-color: #fff;
  border-radius: 8px;
  box-shadow: 10px 5px 5px #000;
  margin-bottom: 1px solid #ccc;

  ${(prop) =>
    prop.keySide % 2 == 0
      ? css`
          flex-direction: row-reverse;
        `
      : css`
          flex-direction: row;
        `}

  justify-content: space-around;
  margin: 10px;
`;

export const TitleCard = styled.Text`
  font-size: 20px;
  color: #442d2d;
  font-weight: bold;
  text-align: center;
`;
export const ImageCard = styled.Image`
  width: 80px;
  height: 80px;
  border-radius: 40px;
  margin-left: 10px;
`;
export const OrderTitleCard = styled.Text`
  font-size: 14px;
  color: #442d2d;
`;
